import React, { useState, useEffect } from 'react';
import {
    FlatList,
    StyleSheet,
    View,
    Text
} from 'react-native';
import {Metrics} from '../asset'
import { Touchable} from '../component'
import { dataTable } from '../data/data'



const Table: () => React$Node = () => {
    const [dataList, setDataList] = useState([])

    useEffect(() => {
        // Your code here
        addData()

      }, []);

   
const addData = () => {
    var data =[]
    for (var i=0; i < dataTable.length; i++) {
        data.push(dataTable[i])
    }
    setDataList(dataList.concat(data))
}

const renderFooter = () => {
    return (
        <View style={{backgroundColor : 'red', height : 60}}>

            <Touchable
                onPress={() => addData()}>

                <View style={{ backgroundColor: 'red', alignItems : 'center', justifyContent : 'center', height : 60 }}>
                    <Text style={{ color: 'white' }}>Click Me For LOAD MORE</Text>
                </View>
            </Touchable>
        </View>
    )
}


    return (
        <>
            <View>
                

                <View>
                    <FlatList
                        data={dataList}
                        renderItem={({ item, index }) => {
                            return (
                                <View style={styles.viewRow}>
                                    <View style={styles.viewRow1}>
                                        <Text style={{ color: 'white' }}>{index + 1}</Text>
                                    </View>
                                    <View style={styles.viewRow2}>
                                        <Text style={{ color: 'white' }}>{item.title + " " + index}</Text>
                                    </View>
                                    <View style={styles.viewRow3}>
                                        <Text style={{ color: 'white' }}>{item.address + " " + index}</Text>
                                    </View>
                                </View>)
                        }}
                        ItemSeparatorComponent={() => <View style={{height: 0.5,
                            backgroundColor: 'rgba(0,0,0,0.4)',}} />}
                        keyExtractor={item => item.id}
                        onEndReachedThreshold={0.4}
                        ListFooterComponent={renderFooter()}
                    />
                </View>
            </View>
        </>
    )
}

const styles = StyleSheet.create({
    viewRow: {
        flex: 1, flexDirection: 'row', flexWrap: 'wrap'
    },
    viewRow1: {width: Metrics.screenWidth/7, height: 50, backgroundColor: 'powderblue', justifyContent : 'center', alignItems : 'center'},
    viewRow2: {width: Metrics.screenWidth/2.34, height: 50, backgroundColor: 'skyblue', justifyContent : 'center', alignItems : 'center'},
    viewRow3: {width: Metrics.screenWidth/2.34, height: 50, backgroundColor: 'steelblue', justifyContent : 'center', alignItems : 'center'},
});

export default Table;