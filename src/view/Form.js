
import React, { useState } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    TextInput,
    Image,
    Alert
} from 'react-native';

import { Touchable, Calendar, Camera} from '../component'
import { getIcon, customColors } from '../asset'
import moment from 'moment';

import {
    Colors
} from 'react-native/Libraries/NewAppScreen';
import DropDownPicker from 'react-native-dropdown-picker';
import { dataProvince } from '../data/data'

const Form: () => React$Node = ({ navigation }) => {
    const [province, setProvince] = useState(null)
    const [firstName, setFirstName] = useState(null)
    const [lastName, setLastName] = useState(null)
    const [district, setDistrict] = useState([])
    const [districtValue, setDistrictValue] = useState(null)
    const [constituency, setConstituency] = useState([])
    const [constituencyValue, setConstituencyValue] = useState(null)
    const [village, setVillage] = useState([])
    const [villageValue, setVillageValue] = useState(null)
    const [salary, setSalary] = useState(null)
    const [date, setDate] = useState(null)
    const [openCalendar, setOpenCalendar] = useState(false)
    const [openCamera, setOpenCamera] = useState(false)
    const [removeCamera, setRemoveCamera] = useState(false)
    const [picture, setPicture] = useState(null)

    const saveProvince = (item) => {
        setProvince(item.value)
        setDistrict(item.kota)

    }

    const saveDistrict = (item) => {
        setDistrictValue(item.value)
        setConstituency(item.kecamatan)

    }

    const saveConstitunce = (item) => {
        setConstituencyValue(item.value)
        setVillage([{ label: 'A', value: 'A' },
        { label: 'B', value: 'B' },
        { label: 'C', value: 'C' },
        { label: 'D', value: 'D' }])
    }

    const onDateChange = (date) => {
        setDate (date)
        setOpenCalendar (!openCalendar)
    }

    const opensCamera = () => {

        if (removeCamera){
            setRemoveCamera(!removeCamera)
            setPicture(null)

        } else {
            setOpenCamera(!openCamera)
        }

    }
    const createdAlert = (title, msg) =>
        Alert.alert(
            title,
            msg,
            [
                {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                { text: "OK", onPress: () => console.log("OK Pressed") }
            ],
            { cancelable: false }
        );

    const saveForm = () => {

        if (firstName && lastName && districtValue && date && province && constituencyValue && villageValue && salary && picture){
            createdAlert("Saved", "Saved Success")
        } else {
            createdAlert("Failed", "Please Checkh your Form")
        }

    }

    const goToTable = () => {
        navigation.navigate('Table')
    }

    return (
        <>
            <SafeAreaView>
                <ScrollView
                    contentInsetAdjustmentBehavior="automatic"
                    style={styles.scrollView}>
                    <View style={styles.body}>

                        <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 16 }}>

                            <Image
                                source={picture ? picture : {uri : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAArlBMVEV2wq////9PXXNyfY9loZLg4NFrvqnv9/VndIe6ua0/UGk8TWb29/hqq5vd3+JanIvj5ejU4+Dn59vm6eXFxbiZ0MJ/xrTY7Ofg8OySzb7L5t+y29Cl1clvv6z0+vjo9PG/4dhtsaCbzLqJyrphbYFTYXaVzsC639Xs7vB/iJhaZ3y92NFfopGdwbjH29WqycFzqZustaixvrHQ3My61MTJ3tGPl6Wboq6Ou7BxuaeCHoC1AAALUUlEQVR4nNWd3WLbNhKFyZShsk1dxbslJVGEJEepnTSuk7bpNn7/FyuoX1IigDODAUGfi1w4lojPM5gBMCCQpMG1Ws+qzbIuFtuyTBqV5XZR1MtNNVuvwj8+Cfnl6+qhKFWe50or6ar5UfM/ZfFQrUM2IhThuqq3Kr8Gu1ZDqso6GGYIwvWmaOCcbF3OXBWbEJTihLM6odK1KJN6Jt0gUcJVtdA9i0V3oszzRSUafwQJNZ4f3ZlyUck1S4pwXmsXk5PK67lQy2QIN6WQ+dqM5UakbQKEk6U43hGynoyAcF0E4tszFt7O6kk4l4ouZsatJ6MX4XwrGV1M8mT0IFzfD8G3Y7z3GOzwCevA/tllrAcn3AzJlzT9kZs7eITzcli+HWPJ644swnqoDtgVz1UZhDPm1MFfSjFmHnTCIo4B98qL4ITzJJYB91IJ1YxEwmVMA+6VLwMSriKE0GupkjRDphDOxsDXiBRwCIQj8NCjKJ6KEw42CkWU34sTjqMLnoV3RpBwPS6+Rgqcb2CEszF56FE5Fm8gwmqMgBoRWnNECDfjBNSIyIwKIHwYK6BGfJAgHFEavBaQGJ2EowZEEF2EI3bRvZyO6iAcbZA5yxVu7IQjTRNdOZKGlXCUif5a9tRvI1y/DECNaBvAWQhX4xuLmqQsw3ALYRm73QSVHMJ7swl3O2GGl2UVU5nni0ZCc6ZXqq7m68nQWs+r2sxozvwmQmMYVaXgLgKyKuNE3BhQDYTGKIOMdYPKOMgyRRsDoSnKKKkdEnzNTX98Q7TpJ1wavkUJ7Bzw1sTUuP6u2Eto6oR5fAs2mpOa10toctHYffCoB5Oj9v1y3w8LwxdY0urAMoQJ1VeZ6iE0+aiKmSa6qgw26EsZPYQmF1DhWw6L0MZrwtoUqjo15o8/Q3onp4/0RvYTmgJVx0k/fnj7I6RXb+T06adzA0xu2hNPrwiNM4r2Zz+8QvXmBzl9AuxwHQ4vCTfmse15mvnxLUz4SpDwzdlRzZPzq2WbC8KVeVqfn8czP/+IE34QJHx3asHE0k47oakH8wkFjYgRXgabLqFtZYZL+NvAhJerNl1Cy7yeTSgXbEDCi/l+h9AcoXwIfx2Y8CJjdAi3lo/xCcWMiBImWxOh1YQehFLBBibsGLFNuLB9yodQKGPAhMmin9BuQh9CISPihG0jtghN00IBQpmMgRO2J4oJ+iEvQplggxO2G3smtAxn/AlFMgaBsDWwORO6Ck1ehCLBhkDYGp2eCM2TChFCiWBDIVSbK0JnpcmTUMCIFMLzPPFI6EgV/oQCRiQRnhLGkdAVZ/wJ/TMGifAUa46E7oK2L6F/xiARnmLNgdC4siNI6O2nNMLjytmB0D4kFSL0DTY0wuPgdE9oWZ4RJPQ1IpEwX7UIASeVIPQMNkTCg5smqJNKEHoGGyLhwU0TMJLKEPoNT6mE+ZkQ2t0lQehnRDLh7EToTvdShF7Bhkq4T/o7QuC3pQh9MgaVcF8Tbv7BdujJEPoYkUy4WxtuCJ0TJ0lCj4xBJtxNoRpCxwKNLKFHsKETFgdCbJelFCE/Y9D7odoTghtlpQj5RqQTNh0xwYZskoTsYMOwYbUjhLKhJCE3YzAI6x2htR4TgpBrRDphU6NJ0EAjScjMGAxC1RCiO/IFCZnBhkGoQ02CBhpRQl7G4Niw0oTGjX4BCXnBhkP4oAmxEY0wISvYcAgLTYi+ViFLyDEigzApNSH6ZowsIceIHEKVJsgyWwhCRsbgEOarBH59S5iQkTFYhOsEfgNPmpDupyzCWYKmQ3lCcrBh9cMqwSb4IQjJRmQRbhLTyyMDEFKDDYtwmYBzpxCE1GDDIqyTAvzVEITE4SmHUPMhJYtQhEQjsggXCTj/DUNICzYswm0Cv+0bhJCUMViEZWRCkhGZhLDCEFIyBouQoDCElGDzQgkJGeOFEhKM+FIJ8WDDJIwcS18RMsbLzBaNAhPGHdPshGYM5pgm6rj0IDDYMMelMecWR4EZgzm3iDk/PAkLNsz5YcQ5fkvhCJcx12lagozIXKeJt9bWUTDCKuJ6aUdIxmCul0Zb874QkDGYa96x6hZXCkS4ilZ7upI72DBrT5Hqhz0KQlhGqwH3yBlsuDXgKHX8XrmCDbeOH2MvRr9cw1PuXowY+2kMchiRu58mwp4oo+RtSNrXxju9hSR7xviAnN7S1Za0N7F1ytQPMYz45v/nBoBjzcPeRDDUqM/nB6Sf3oaR7dSlf1rP/wy2uaLsEU6+p239NLQ6T/+ONfmwRxgNNY/pePSINfmwzxsd1Tx+dj13MH3GCE979cFpfpbFBjspyzDC4/sWYEe8mf4em+yg36c3UItP78xg7z0lN9n0KTLaXk/TDCM8vfcEZsS7LJvexmXb6XaaZXdIg1vvrmH58znLxmBFbcEse0Ya3Hr/EHqHtAk1GvG76KXZZK2+N4BYoGm9Qwq9B7xz04bxS7zDhCdfdnyYk3beAwYHbtkecfr1y9MtqP+5hX7V05ev0z0gmCva73KDC27PWXaERPUft/AvOz4e6oXd9/FBN9UJg6hHgPCR+qVgquicqQAvZVARH/9wAv5BJQQB8+65GGA0PUabmIRYlLk62wSdBrf6YiRCrA/2nE/jPmPoJJIZAULK16EG7DljiHYjyd3dDag/f3HpT/Sr7nC8pOecKHQKRZT667VLf4V58PVZX3CsoT0oFmHPeW14rKFIOQFfvw7y3GUPYZDNcCVAGOJOot5zE+EiFEXvAcL38o/tP/uSkjDgJ30DCL/J/2Xbx0HjZ9ByhHTDEB3RcAZtACMuIELxv6zxHGG0RgMLyBWNxPOF8SxoaSMqJM40ei+LaDnP23omO1kKSRV7iV6JbTuTXTQnEgBlEfOJhVBqYKO0kERx1rfmIzLPtt6N4DU6zdW2OKhe/v1fqv5eHj9dbJVPM1I7IXuKoUrsInBMM7bbuu4oYd9c2XuZlI+4Y0jnPTPcjLG4/B5v8cYBwF1BvGCTy6/1w5sm20Lue4KL3p0vlvbRRhw/he7s4txzHORKNnh/9lngvWuMP579VmWm6H9p9O48tCY8PkL4/kN6PFWXSUhC5NRMuMPSeA+pUSEuf6RmZso9pPRvz+UL/LdURyLdJUu+dfx5Kl0YnkzRCsVBxPuAqd38Tnqbxu2UUKNoRL3T2XYvd59usmz69WkudAX3/OnrFK4THgDJ93IT5/u7yilerobq2RRCzt3qtGhDLJwKV9KssdxCSIo2AQgJTzdFGQchvLW2EXkPg1MEJ7248RAnJAVUcUICoHXMaCVMq2hWpFjQPrGxE6YbAuLzjRTkzQ0h218tzNAI04cgpWFB5a4L312ExMw/uMyZHiYcN6IbECAcs6M6XRQjJIWbQeUKMjAhKWkMKEeaoBBylkzCC1wcwgjTdZBtPV5StqEanTBdiRYx/aVKdJUdJdTzxTF5am6eD/IJx5QYgTTIIUxnY3FURVmAphCOpDPiXZBOOApPpXgogzCdJ3HNqBJqiYRKmKZFTDPm9EIlnVAHnFhmVKQQwydM0zqOGfPrEnYownReDs+oyt7iWSDCZkY1rKsqaKYkSdi46nCMiuegnoTpZLCRan7vUbvzINTdcTsEY77ldUAJQs24COyrKl948XkT6rlxEZBR5YUnnwCh7o/LQIw6vgjUzgUItTalOKTKS5ktLDKEukPWPrtee/hqb/c8SIpQq5KKOjq6CO6TEyTUqu5zT0qVazzRrZyyhFqzOsmZkw+lHb0W3yEnTqi13hSKSqnpVLEBl0BJCkHYaFLV2wbTzakauLKuQtA1CkW403r2UJS6Z+U9L1M0P2r+pywegsHtFJRwr9V6Vm2WdbHYlvstOmW5XRT1clPN1gMcBfMvWcKMX2+A1HUAAAAASUVORK5CYII='}}
                                style={{ width: 200, height: 200, borderRadius: 200 / 2, borderColor: customColors.primaryColor, borderWidth: 1 }}
                            />
                            <Touchable
                                onPress={() => opensCamera()}>
                                <View style={{ alignItems: 'center', justifyContent: 'center', width: 60, height: 60, borderRadius: 60 / 2, borderColor: customColors.primaryColor, borderWidth: 1, marginTop: -30, backgroundColor: 'white' }}>

                                    <Image
                                        source={removeCamera ?  getIcon('ic_remove') : getIcon('ic_camera')}
                                        style={{ width: 30, height: 30 }}
                                    />


                                </View>
                            </Touchable>

                        </View>


                        <TextInput
                            style={{
                                height: 40, borderColor: 'gray', borderWidth: 1, borderTopLeftRadius: 10, borderTopRightRadius: 10,
                                borderBottomLeftRadius: 10, borderBottomRightRadius: 10, margin: 16, padding: 8
                            }}
                            onChangeText={text => setFirstName(text)}
                            value={firstName}
                            placeholder={"First Name"}
                        />

                        <TextInput
                            style={{
                                height: 40, borderColor: 'gray', borderWidth: 1, borderTopLeftRadius: 10, borderTopRightRadius: 10,
                                borderBottomLeftRadius: 10, borderBottomRightRadius: 10, margin: 16, padding: 8, marginTop: 0
                            }}
                            onChangeText={text => setLastName(text)}
                            value={lastName}
                            placeholder={"Last Name"}
                        />

                        <Text
                            style={{
                                borderColor: 'gray', borderWidth: 1, borderTopLeftRadius: 10, borderTopRightRadius: 10,
                                borderBottomLeftRadius: 10, borderBottomRightRadius: 10, margin: 16, padding: 8, marginTop: 0
                            }}
                            onPress={() => setOpenCalendar(!openCalendar)}>
                            {date ? moment(date).format("DD MMM YYYY") : "Your Brithday"} </Text>

                        <DropDownPicker
                            items={dataProvince}
                            placeholder={"Select your Province"}
                            defaultValue={province}
                            containerStyle={{ height: 40, margin: 16, marginTop: 0 }}
                            style={{
                                backgroundColor: '#fafafa', borderTopLeftRadius: 10, borderTopRightRadius: 10,
                                borderBottomLeftRadius: 10, borderBottomRightRadius: 10, borderColor: 'gray'
                            }}
                            dropDownStyle={{ backgroundColor: '#fafafa' }}
                            onChangeItem={item => saveProvince(item)}
                        />

                        {district.length !== 0 && <DropDownPicker
                            items={district}
                            placeholder={"Select your District"}
                            defaultValue={districtValue}
                            containerStyle={{ height: 40, margin: 16, marginTop: 0 }}
                            style={{
                                backgroundColor: '#fafafa', borderTopLeftRadius: 10, borderTopRightRadius: 10,
                                borderBottomLeftRadius: 10, borderBottomRightRadius: 10, borderColor: 'gray'
                            }}
                            dropDownStyle={{ backgroundColor: '#fafafa' }}
                            onChangeItem={item => saveDistrict(item)}
                        />}

                        {constituency.length !== 0 && <DropDownPicker
                            items={constituency}
                            placeholder={"Select your Constituency"}
                            defaultValue={constituencyValue}
                            containerStyle={{ height: 40, margin: 16, marginTop: 0 }}
                            style={{
                                backgroundColor: '#fafafa', borderTopLeftRadius: 10, borderTopRightRadius: 10,
                                borderBottomLeftRadius: 10, borderBottomRightRadius: 10, borderColor: 'gray'
                            }}
                            dropDownStyle={{ backgroundColor: '#fafafa' }}
                            onChangeItem={item => saveConstitunce(item)}
                        />}

                        {village.length !== 0 && <DropDownPicker
                            items={village}
                            placeholder={"Select your Village"}
                            defaultValue={villageValue}
                            containerStyle={{ height: 40, margin: 16, marginTop: 0 }}
                            style={{
                                backgroundColor: '#fafafa', borderTopLeftRadius: 10, borderTopRightRadius: 10,
                                borderBottomLeftRadius: 10, borderBottomRightRadius: 10, borderColor: 'gray'
                            }}
                            dropDownStyle={{ backgroundColor: '#fafafa' }}
                            onChangeItem={item => setVillageValue(item.value)}
                        />}

                        <TextInput
                            style={{
                                height: 40, borderColor: 'gray', borderWidth: 1, borderTopLeftRadius: 10, borderTopRightRadius: 10,
                                borderBottomLeftRadius: 10, borderBottomRightRadius: 10, margin: 16, padding: 8, marginTop: 0
                            }}
                            keyboardType={'numeric'}
                            onChangeText={text => setSalary(text)}
                            value={salary}
                            ty
                            placeholder={"Employee Salary:"}
                        />

                        <Touchable
                            onPress={() => saveForm()}>

                            <View style={{
                                alignItems: 'center', justifyContent: 'center', backgroundColor: customColors.primaryColor, borderWidth: 1, marginTop: 0, height: 50, margin: 16, borderTopLeftRadius: 10, borderTopRightRadius: 10,
                                borderBottomLeftRadius: 10, borderBottomRightRadius: 10
                            }}>

                                <Text
                                    style={{ color: "white" }}>
                                    Save </Text>

                            </View>

                        </Touchable>

                        <Touchable
                            onPress={() => goToTable()}>

                            <View style={{
                                alignItems: 'center', justifyContent: 'center', backgroundColor: 'red', borderWidth: 1, marginTop: 8, height: 50, margin: 16, borderTopLeftRadius: 10, borderTopRightRadius: 10,
                                borderBottomLeftRadius: 10, borderBottomRightRadius: 10
                            }}>

                                <Text
                                    style={{ color: "white" }}>
                                    Table List </Text>

                            </View>

                        </Touchable>

                        <Calendar
                        selectedStartDate = {date}
                            openCalendar={openCalendar}
                            closeCalendar={(openCalendar) => setOpenCalendar(openCalendar)}
                            onDateChange={(date) => onDateChange(date)}
                        />

                        <Camera
                        openCamera = {openCamera}
                        source = {(source) => {
                            setPicture(source)
                            setOpenCamera(!openCamera)
                            setRemoveCamera(!removeCamera)}}
                        />
               

                    </View>
                </ScrollView>
            </SafeAreaView>
        </>
    );
};

const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: Colors.lighter,
    },
    body: {
        backgroundColor: Colors.white,
    },
});

export default Form;
