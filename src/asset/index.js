import { Dimensions} from 'react-native'

const { width, height } = Dimensions.get('window')
const screenWidth = width < height ? width : height
const screenHeight = width < height ? height : width


function getIcon(name) {
    switch (name) {
        case 'ic_camera': return require('./icon/ic_camera.png')
        case 'ic_remove': return require('./icon/ic_remove.png')
    }
}


const customColors = {
    primaryColor : '#1296db',
    black_transparent: 'rgba(0, 0, 0, 0.5)',
    white: '#ffffff',
}

const Scale = (value) => {
    const { width } = Dimensions.get('window')
    const guidelineBaseWidth = 350;

    return width / guidelineBaseWidth * value
}


const Metrics = {
    navBot: Scale(50),
    screenWidth,
    screenHeight,
}



export {getIcon, customColors, Metrics, Scale}