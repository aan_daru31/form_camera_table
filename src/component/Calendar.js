import React, { useState } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    Modal,
    Alert
} from 'react-native';
import {customColors} from '../asset'
import {Touchable} from '../component'

import CalendarPicker from 'react-native-calendar-picker';

const Calendar = (props) => {

    const [date, setDate] = useState(null)
    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={props.openCalendar}
        >
 
            <View
                style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: customColors.black_transparent }}>

                <View style={{ backgroundColor: customColors.white, margin : 16}}>
                    <CalendarPicker
                    maxDate = {new Date()}
                      onDateChange={(date) => setDate(date)}
                    />

                    <View
                        style={{ backgroundColor: 'gray', height: 1, margin: 16, marginBottom : 0}} />


                    <View style={{ height: 90, flexDirection: 'row', justifyContent : 'flex-end', marginTop : 16 }}>

                        <View style={{ width: 120, height: 50 }}>
                            <Touchable
                                onPress={() => props.closeCalendar(false)}>

                                <View style={{
                                    alignItems: 'center', justifyContent: 'center', backgroundColor: customColors.primaryColor, borderWidth: 1, marginTop: 8, height: 50, margin: 16, borderTopLeftRadius: 10, borderTopRightRadius: 10,
                                    borderBottomLeftRadius: 10, borderBottomRightRadius: 10
                                }}>

                                    <Text
                                        style={{ color: "white" }}>
                                        Cancel </Text>

                                </View>

                            </Touchable>
                        </View>

                        <View style={{ width: 120, height: 50 }}>
                            <Touchable
                                onPress={() => props.onDateChange(date)}>

                                <View style={{
                                    alignItems: 'center', justifyContent: 'center', backgroundColor: customColors.primaryColor, borderWidth: 1, marginTop: 8, height: 50, margin: 16, borderTopLeftRadius: 10, borderTopRightRadius: 10,
                                    borderBottomLeftRadius: 10, borderBottomRightRadius: 10
                                }}>

                                    <Text
                                        style={{ color: "white" }}>
                                        Ok </Text>

                                </View>

                            </Touchable>
                        </View>

                    </View>

                </View>

            </View>



        </Modal>
    )
}

export default Calendar