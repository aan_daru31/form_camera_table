import React, { useState } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    Modal,
    Alert
} from 'react-native';
import {customColors} from '../asset'
import {Touchable} from '../component'

import ImagePicker from 'react-native-image-picker';

const Camera = (props) => {

    const options = {
        title: 'Select Picture',
        customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
        storageOptions: {
          skipBackup: true,
          path: 'images',
        }
    }
    return (
<>

       {props.openCamera && <View
            style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: customColors.black_transparent }}>

                {ImagePicker.showImagePicker(options, (response) => {
                    console.log('Response = ', response);

                    if (response.didCancel) {
                        console.log('User cancelled image picker');
                    } else if (response.error) {
                        console.log('ImagePicker Error: ', response.error);
                    } else if (response.customButton) {
                        console.log('User tapped custom button: ', response.customButton);
                    } else {

                        const source = { uri: response.uri };

                        props.source(source)
                    }
                })}

        </View>}
        </>
    )
}

export default Camera