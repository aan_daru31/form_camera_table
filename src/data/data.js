const dataProvince = [
    {
        label: "Aceh",
        value: "Aceh",
        kota :[
            {label: "Kota Banda Aceh",value: "Sumatera Utara",
            kecamatan : [{
                value:"110101",
                label:"Bakongan"},
                {value:"110102",
                label:"Kluet Utara"},
                {value:"110103",
                label:"Kluet Selatan"},
                {value:"110104",
                label:"Labuhan Haji"},]},
            {label: "Kota Sabang",value: "Sumatera Utara",
            kecamatan : [{
                value:"110101",
                label:"Bakongan"},
                {value:"110102",
                label:"Kluet Utara"},
                {value:"110103",
                label:"Kluet Selatan"},
                {value:"110104",
                label:"Labuhan Haji"},]},
            {label: "Kota Lhokseumawe",value: "Sumatera Utara",
            kecamatan : [{
                value:"110101",
                label:"Bakongan"},
                {value:"110102",
                label:"Kluet Utara"},
                {value:"110103",
                label:"Kluet Selatan"},
                {value:"110104",
                label:"Labuhan Haji"},]},
            {label: "Kota Langsa",value: "Sumatera Utara",
            kecamatan : [{
                value:"110101",
                label:"Bakongan"},
                {value:"110102",
                label:"Kluet Utara"},
                {value:"110103",
                label:"Kluet Selatan"},
                {value:"110104",
                label:"Labuhan Haji"},]},   
            {label: "Kota Subulussalam",value: "Sumatera Utara",
            kecamatan : [{
                value:"110101",
                label:"Bakongan"},
                {value:"110102",
                label:"Kluet Utara"},
                {value:"110103",
                label:"Kluet Selatan"},
                {value:"110104",
                label:"Labuhan Haji"},]}]
        
    },

    {
        label: "Sumatera Utara",
        value: "Sumatera Utara",
        kota: [
            {
                label: "KotaMedan",
                value: "Sumatera Utara",
                kecamatan : [{
                    value:"110101",
                    label:"Bakongan"},
                    {value:"110102",
                    label:"Kluet Utara"},
                    {value:"110103",
                    label:"Kluet Selatan"},
                    {value:"110104",
                    label:"Labuhan Haji"},]
            },
            {
                label: "KotaPematang Siantar",
                value: "Sumatera Utara",
                kecamatan : [{
                    value:"110101",
                    label:"Bakongan"},
                    {value:"110102",
                    label:"Kluet Utara"},
                    {value:"110103",
                    label:"Kluet Selatan"},
                    {value:"110104",
                    label:"Labuhan Haji"},]
            },
            {
                label: "KotaSibolga",
                value: "Sumatera Utara",
                kecamatan : [
                    {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
                ]
            },
            {
                label: "KotaTanjung Balai",
                value: "Sumatera Utara",
                kecamatan : [
                    {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
                ]
            },
            {
                label: "KotaBinjai",
                value: "Sumatera Utara",
                kecamatan : [
                    {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
                ]
            }
        ]
        
    },

    {
        label: "Sumatera Barat",
        value: "Sumatera Barat",
        kota: [
            { label: "KotaPadang", value: "Sumatera Utara",kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            { label: "KotaSolok", value: "Sumatera Utara",kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            { label: "KotaSawhlunto", value: "Sumatera Utara",kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            { label: "KotaPadang Panjang", value: "Sumatera Utara",kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            { label: "KotaBukittinggi", value: "Sumatera Utara",kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            { label: "KotaPayakumbuh", value: "Sumatera Utara",kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            { label: "KotaPariaman", value: "Sumatera Utara",kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            { label: "Kab.Pasaman Barat", value: "Sumatera Utara",kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            { label: "Kab.Solok Selatan", value: "Sumatera Utara",kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            { label: "Kab.Dharmasraya", value: "Sumatera Utara",kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            { label: "Kab.Kepulauan Mentawai", value: "Sumatera Utara",kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            { label: "Kab.Pasaman", value: "Sumatera Utara",kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            { label: "Kab.Lima Puluh Kota", value: "Sumatera Utara",kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            { label: "Kab.Agam", value: "Sumatera Utara",kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            { label: "Kab.Padang Pariaman", value: "Sumatera Utara",kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            { label: "Kab.Tanah Datar", value: "Sumatera Utara",kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            { label: "Kab.Sijunjung", value: "Sumatera Utara",kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            { label: "Kab.Solok", value: "Sumatera Utara" ,kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ]},
            { label: "Kab.Pesisir Selatan", value: "Sumatera Utara",kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
        ],
        
    },

    {
        label: "Riau",
        value: "Riau",
        kota: [
            {label : "KotaPekan Baru",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            {label : "KotaDumai",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            {label : "Kab.Kepulauan Meranti",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            {label : "Kab.Kuantan Singingi",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            {label : "Kab.Siak",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            {label : "Kab.Rokan Hilir",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            {label : "Kab.Rokan Hulu",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            {label : "Kab.Pelalawan",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            {label : "Kab.Indragiri Hilir",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            {label : "Kab.Bengkalis",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            {label : "Kab.Indragiri Hulu",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            {label : "Kab.Kampar",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
        ]
    },

    {
        label: "Jambi",
        value: "Jambi",
        kota: [
            {label : "KotaJambi",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            {label : "KotaSungai Penuh",value: "Sumatera Utara" , kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ]},
            {label : "Kab.Tebo",value: "Sumatera Utara" , kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ]},
            {label : "Kab.Bungo",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            {label : "Kab.Tanjung Jabung Timur",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            {label : "Kab.Tanjung Jabung Barat",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            {label : "Kab.Muaro Jambi",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            {label : "Kab.Batanghari",value: "Sumatera Utara" , kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ]},
            {label : "Kab.Sarolangun",value: "Sumatera Utara" , kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ]},
            {label : "Kab.Merangin",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            {label : "Kab.Kerinci",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
        ]
    },

    {
        label: "Sumatera Selatan",
        value: "Sumatera Selatan",
        kota: [
            {label : "KotaPalembang",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            {label : "KotaPagar Alam",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            {label : "KotaLubuk Linggau",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            {label : "KotaPrabumulih",value: "Sumatera Utara" , kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ]},
            {label : "Kab.Musi Rawas Utara",value: "Sumatera Utara" , kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ]},
            {label : "Kab.Penukal Abab Lematang Ilir",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            {label : "Kab.Empat Lawang",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            {label : "Kab.Ogan Ilir ",value: "Sumatera Utara" , kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ]},
            {label : "Kab.Ogan Komering Ulu Selatan ",value: "Sumatera Utara" , kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ]},
            {label : "Kab.Ogan Komering Ulu Timur ",value: "Sumatera Utara" , kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ]},
            {label : "Kab.Banyuasin",value: "Sumatera Utara" , kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ]},
            {label : "Kab.Musi Banyuasin",value: "Sumatera Utara" , kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ]},
            {label : "Kab.Musi Rawas",value: "Sumatera Utara" , kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ]},
            {label : "Kab.Lahat",value: "Sumatera Utara" , kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ]},
            {label : "Kab.Muara Enim",value: "Sumatera Utara" , kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ]},
            {label : "Kab.Ogan Komering Ilir",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            {label : "Kab.Ogan Komering Ulu", value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
        ],
        
    },

    {
        label: "Bengkulu",
        value: "Bengkulu",
        kota: [
            {label : "KotaBengkulu",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            {label : "Kab.Bengkulu Tengah",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            {label : "Kab.Kepahiang ",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            {label : "Kab.Lebong",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            {label : "Kab.Muko Muko",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            {label : "Kab.Seluma",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            {label : "Kab.Kaur",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            {label : "Kab.Bengkulu Utara",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            {label : "Kab.Rejang Lebong",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
            {label : "Kab.Bengkulu Selatan",value: "Sumatera Utara", kecamatan : [
            {value:"120101",label:"Barus"},{value:"120102",label:"Sorkam"},{value:"120103",label:"Pandan"},{value:"120104",label:"Pinangsori"}
        ] },
        ]
    }
]

const dataTable = [
    {
        title: 'Rumah Indah',
        address: 'Tangerang'
    },
    {
        title: 'Rumah Bagus',
        address: 'Tangerang'
    },
    {
        title: 'Rumah Mewah',
        address: 'Tangerang'
    },
    {
        title: 'Rumah Mewah Sekali',
        address: 'Tangerang'
    },
    {
        title: 'Rumah Mewah Biasa',
        address: 'Tangerang'
    },
    {
        title: 'Rumah Indah',
        address: 'Tangerang'
    },
    {
        title: 'Rumah Bagus',
        address: 'Tangerang'
    },
    {
        title: 'Rumah Mewah',
        address: 'Tangerang'
    },
    {
        title: 'Rumah Mewah Sekali',
        address: 'Tangerang'
    },
    {
        title: 'Rumah Mewah Biasa',
        address: 'Tangerang'
    },
    {
        title: 'Rumah Indah',
        address: 'Tangerang'
    },
    {
        title: 'Rumah Bagus',
        address: 'Tangerang'
    },
    {
        title: 'Rumah Mewah',
        address: 'Tangerang'
    },
    {
        title: 'Rumah Mewah Sekali',
        address: 'Tangerang'
    },
    {
        title: 'Rumah Mewah Biasa',
        address: 'Tangerang'
    },
    {
        title: 'Rumah Indah',
        address: 'Tangerang'
    },
    {
        title: 'Rumah Bagus',
        address: 'Tangerang'
    },
    {
        title: 'Rumah Mewah',
        address: 'Tangerang'
    },
    {
        title: 'Rumah Mewah Sekali',
        address: 'Tangerang'
    },
    {
        title: 'Rumah Mewah Biasa',
        address: 'Tangerang'
    },
]

export { dataProvince, dataTable }